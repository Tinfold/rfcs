- Feature Name: asset-pipeline
- Start Date: 2018-07-16
- Tracking Issue: 0007

# 1. Summary
[summary]: #summary

We need an organized process to decide on which assets the game needs and bring them into the game, as every individual contributor holds different levels of knowledge of game vision as well as different experience levels in group projects. This is my proposal.

# 2. Motivation
[motivation]: #motivation

The goal of of this feature is to ensure that assets we use are necessary for the vision of the game, technically applicable to the Veloren code, match the style of the game, match a certain level of creativity and artistic skill and can be animated when necessary.

# 3. Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

I've organized a four step process

1) Relevant checklists for each version are decided by core developers during roadmapping, they give voxel artists an idea of where to focus.

2) Artists make assets. Post in #voxel-art. Based on feedback, continuously make necessary tweaks until satisfaction. Split into limbs if necessary. Added to "pending" folder of asset repository on Gitlab by someone with access.

3) Lead voxel artists post these .vox files into #asset-evaluation at a later point, at specified meetup times. With core developers and other artists, it is decided if the art works on a technical level to meet guidelines, if it's good, if it fits the style of the game and if it can be animated if necessary. This is when the palette and scaling are examined, as well as use of special palette materials and whether or not the model has been fit within Magicavoxel, among other factors. Again, necessary tweaks are made.


4) Configs are created by voxel artists as well as Game Design for certain assets. Notes are made in configs about the specific use of the item in game. The asset is given a base ID and placed into the main, 'in-use' portion of the asset repository.

Current Google Drive assets will be moved into pending. I am strongly in favor of the above permissions for #asset-evaluation, allowing for everyone to read, everyone with asset credentials to post, and having lead artists and core devs to be the contributors to the conversation. I think it is important for leaders to be transparent and for people to understand and possibly ask questions during the process about reasons why their model wasn't included. This channel will however be strictly moderated, the conversation will not go off topic. Extensive, continued input from artists who are not long time contributors to the game won't be tolerated. There are many channels and other times which allow for normal feedback and basic questions, but this channel is specifically dedicated to solving tough problems and sometimes making sacrifices. 

# 4. Drawbacks
[drawbacks]: #drawbacks

The process is several steps long and could be strenuous to complete for the hundreds of models that are necessary. By ensuring that we are organized in the days in which we complete step 3, I think that we can push large groups of assets into near game-ready status at once, and minimize the extent of delays. 

# 5. Rationale and alternatives
[alternatives]: #alternatives

The Gitlab is not only more secure, but also allows direct access into the game. Keeping all assets in this one location is logical. The described process allows for thorough vetting, complete impartiality and palces no emphasis on one individual's vision for the game, completing every step through collaborative process. 

# 8. Unresolved questions
[unresolved]: #unresolved-questions

The exact nature of configs is still being decided. Additional rules need to be stated before we can accurately determine whether or not some assets are eligible for the game. Further discussion of animation is necessary in order to determine exactly what type of factors the evaluators will need to look at. 