**Veloren** || A flexible game design document.  
===============



**Table of Contents** 

[[_TOC_]]  






**Abstract**
-------------------------------------  


The purpose of this document is to provide guidance for all contributors. This document does NOT implement deadlines, nor is it meant to force anyone to do things they don't want to do. Given the open-source nature of the project, you should be able to work on whatever aspect you like. However, we must realize that everyone is contributing to the same **game**. Everything you add should mesh with the big picture. So in essence, this document serves to help each team create a cohesive experience. It also allows newer contributors to see what's going on with the game instead of spamming all the devs. A lot of time is wasted addressing FAQ in the discord about development; if we put all the answers in one convenient place, many issues are solved. We all want to see the game succeed; whenever it does is not important. Let's just make it an eventuality instead of a possibility.  


**Philosophy**
--------------------------------------  

- **Game design is important and the game design team needs to be restructured so that they are directly involved with the development process.**
- The game design team **should** be composed of people who have had experience with making games and dealing with playerbases. 
- A game designer should be able to give direction on mechanics and judge whether or not something is a bad idea and if it will work in the grand scheme. 
- A game designer's main responsibility is to make sure that the core gameplay loops are as rewarding and fun as possible, while also ensuring that all mechanics are sound and combine with eachother enough to make a cohesive gameplay experience. 
- Given the open-source nature of the project, a game designer should not restrict the freedoms of each respective team; they must simply direct it to the benefit of the player.

**Game Design Document**
===========================================

Each team/person responsible for each section should contribute to each section. **When adding new features, each team should consult the Game Design team first.**  
Section guidelines:  
- Under Current State, address the current state of the section as it is in the game as a concise summary (include mechanics, etc) Also include major problems.  
- Under Direction, address the direction that the section should be working towards as a concise summary (include mechanics, etc). Present solutions to major problems.
- Under Related RFCs, hyperlink RFCs relevant to the Direction and Current State.
- Do not write your college thesis anywhere. Reserve large, detailed info for RFCs and link them if you need to.

This way, everyone can be aware of what is going on with each aspect of the game's development. A lot of gray area is eliminated and communication is enhanced. Update the document as often as you see fit. Don't forget to consult the Game Design team when thinking about new features.

*Assets & Visual Design*
-------------------------------------------
**Current State:**  

**Direction:**  

**Related RFCs:**  


*Audio*
-------------------------------------------
**Current State:**  

**Direction:**  

**Related RFCs:**  


*Combat*
-------------------------------------------
**Current State:**  

**Direction:**  

**Related RFCs:**  


*Game Design*
-------------------------------------------
**Current State:**  

**Direction:**  

**Related RFCs:**  


*Rendering/Lighting*
-------------------------------------------
**Current State:**  

**Direction:**  

**Related RFCs:**  


*WorldGen*
-------------------------------------------
**Current State:**  

**Direction:**  

**Related RFCs:**  


*WorldSim*
-------------------------------------------
**Current State:**  

**Direction:**  

**Related RFCs:**  


*Miscellaneous*
-------------------------------------------
**Current State:**  

**Direction:**  

**Related RFCs:**  

